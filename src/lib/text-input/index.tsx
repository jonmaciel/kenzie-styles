import React from 'react'
import styled from 'styled-components'

interface Props {
  type?: string
  label?: string
  value?: string
  onChange?: (value: string) => any
}

const TextInput = ({ type = 'text', label, value, onChange }: Props) => (
  <SimpleFormGroup>
    {label && <SimpleTextLabel>{label}</SimpleTextLabel>}
    <SimpleTextInput
      type={type}
      value={value}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
        onChange && onChange(e.target.value)
      }
    />
  </SimpleFormGroup>
)

export default TextInput

const SimpleFormGroup = styled.div`
  margin-bottom: 1rem;
`

const SimpleTextLabel = styled.div`
  display: block;
  color: red;
`

const SimpleTextInput = styled.input`
  display: inline-block;
  margin-bottom: 0.5rem;
  font-size: 16px;
  font-weight: 400;
  color: rgb(33, 37, 41);
`
