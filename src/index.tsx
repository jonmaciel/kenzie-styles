import * as React from 'react'
import styles from './styles.module.css'
import TextInput from './lib/text-input'

interface Props {
  text: string
}

const ExampleComponent = ({ text }: Props) => {
  return <div className={styles.test}>Example aaaa Component: {text}</div>
}

export { ExampleComponent, TextInput }
