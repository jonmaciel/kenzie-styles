import React from 'react'

import { ExampleComponent, TextInput } from 'kenzie-styles'
import 'kenzie-styles/dist/index.css'

const App = () => {
  return (
    <div>
      <TextInput label='Hello!' />
      <ExampleComponent text='Create React Library Example 😄' />
    </div>
  )
}

export default App
