# kenzie-styles

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/kenzie-styles.svg)](https://www.npmjs.com/package/kenzie-styles) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save kenzie-styles
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'kenzie-styles'
import 'kenzie-styles/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [jonmaciel](https://github.com/jonmaciel)
